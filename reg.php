<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';

//unauthorizedUserRedirect('../login.php');

?>

    <?php setPageTitle('Page Title Here');  require_once 'header.php'; ?>

    <!--    [ Strat Section Title Area]-->
    <section id="login">
        <div class="section-paddings">
            <div class="container">
                <div class="all-reg-frm owl-carousel">
                    <div class="login-content d-table">
                        <div class="login-txt d-table-cell">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <form action="">
                                        <div class="reg-frm">
                                            <div class="row justify-content-end">
                                                <div class="col-lg-6">
                                                    <div class="input-field ">
                                                        <i class="icofont prefix">user_alt_3</i>
                                                        <input id="icon_prefix" type="text" class="validate">
                                                        <label for="icon_prefix">Name</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <i class="icofont prefix">phone</i>
                                                        <input id="icon_telephone" type="tel" class="validate">
                                                        <label for="icon_telephone">Telephone</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <i class="icofont prefix">envelope</i>
                                                        <input id="email" type="email" class="validate">
                                                        <label for="email">Email</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <i class="icofont prefix">institution</i>
                                                        <input id="icon_institution" type="text" class="validate">
                                                        <label for="icon_institution">institution</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <i class="icofont prefix">chair</i>
                                                        <input id="icon_chair" type="text" class="validate">
                                                        <label for="icon_chair">Designation</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <p class="prefix icofont">users_alt_4</p>
                                                        <select class="icons">
                                                          <option class="sing-color" value="" disabled  selected>Select Your Gender</option>
                                                          <option value="" data-icon="assets/img/man.png" class="left">Man</option>
                                                          <option value="" data-icon="assets/img/woman.png" class="left">Woman</option>
                                                          <option value="" data-icon="assets/img/gender2.jpg" class="left">Other</option>
                                                    </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="login-content d-table">
                        <div class="login-txt d-table-cell">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <form action="">
                                        <div class="reg-frm">
                                            <div class="row justify-content-end">
                                                <div class="col-lg-6">
                                                    <div class="input-field ">
                                                        <i class="icofont prefix">github</i>
                                                        <input id="icon_prefix" type="text" class="validate">
                                                        <label for="icon_prefix">portfolio link(ex: GitHub)</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <i class="icofont prefix">play</i>
                                                        <input id="icon_telephone" type="text" class="validate">
                                                        <label for="icon_telephone">Google Playstore account link</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <i class="icofont prefix">handshake_deal</i>
                                                        <input id="icon_telephone" type="text" class="validate">
                                                        <label for="icon_telephone">Why do you want to join?</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="input-field">
                                                        <p class=" icofont prefix">users_alt_4</p>
                                                        <select class="icons">
                                                          <option class="sing-color" value="" disabled  selected>T-shirt size</option>
                                                          <option value=""  class="left">S</option>
                                                          <option value=""  class="left">M</option>
                                                          <option value=""  class="left">XL</option>
                                                          <option value=""  class="left">XXL</option>
                                                    </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="login-content d-table">
                        <div class="login-txt d-table-cell">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <div class="log-frm on-going profession">
                                        <h4>How do you define your technical skill *</h4>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>Android Developer <i class="icofont">android_robot</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                        <input name="group1" type="radio" checked />
                                                        <span>Front end Developer <i class="icofont">monitor</i></span>
                                                    </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>Back end Developer <i class="icofont">web</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>UX Designer <i class="icofont">paint</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>Bot Developer <i class="icofont">facebook_messenger</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>Full stack Developer <i class="icofont">battery_full</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="login-content d-table">
                        <div class="login-txt d-table-cell">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <div class="log-frm on-going profession">
                                        <h4>Your technical skill experience(in year?) *</h4>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>0-1 <i class="icofont">bag_alt</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                        <input name="group1" type="radio" checked />
                                                        <span>1-2 <i class="icofont">bag_alt</i></span>
                                                    </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>2-3 <i class="icofont">bag_alt</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>3-4 <i class="icofont">bag_alt</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>4-5 <i class="icofont">bag_alt</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>More than 5 <i class="icofont">bag_alt</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="login-content d-table">
                        <div class="login-txt d-table-cell">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <div class="log-frm on-going profession">
                                        <h4>How do you define your Android expertise?</h4>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            
                                                            <span>Beginner <i class="icofont">meetme</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                        <input name="group1" type="radio" checked />
                                                        <span>Intermideate <i class="icofont">smugmug</i></span>
                                                    </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>Advanced <i class="icofont">heart_eyes</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="login-content d-table">
                        <div class="login-txt d-table-cell">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <div class="log-frm on-going profession">
                                        <h4>Did you attend any GDG Program before? *</h4>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>Yes <i class="icofont">heart_eyes</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                        <input name="group1" type="radio" checked />
                                                        <span>No <i class="icofont">nerd_smile</i></span>
                                                    </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-right">
                                                <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <!--    [ Strat Section Area]-->
    <!--    [Finish Section Area]-->
    <?php require_once 'footer.php' ?>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function() {
            $('select').formSelect();
        });

    </script>
    <script>


    </script>
