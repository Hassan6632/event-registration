<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/auth-controller.php';
require_once 'app/game-controller.php';

//unauthorizedUserRedirect('../login.php');

$participant = null;

if(isset($_POST['email']))
{
    participant_game();
}

?>

<?php setPageTitle('Game');  require_once 'header.php'; ?>
    <style>
        #step0commands {
            display: none;
        }

        .agile_form {
            padding: 3em;
            background: #fff;
            padding-bottom: 71px !important;
        }
        p.copy-right {
            width: fit-content;
            margin: 0 auto;
            margin-top: 81px;
            margin-bottom: -33px;
        }
        p.copy-right img {
            width: 92px;
            margin-bottom: -4px;
        }

    </style>

    <!--    [ Strat Section Title Area]-->
    <section id="reg-frm">
        <div class="section-padding">
            <div class="main">
                <h1>GDG Dhaka</h1>
                <div class="w3_agile_main_grids">
                    <form id="SignupForm" action="game.php" class="agile_form" method="post">
                        <fieldset>
                            <h3><i class="fas fa-map-marker-alt"></i>I/O Replay Game</h3>

                            <?php if(!empty($participant)): ?>
                            <div class="thanks-messsage">
                                <?php if($participant['attended'] == 1): ?>
                                <div class="checkin-exceptions">
                                    <p><span><?= $participant['name'] ?></span> already checked in .Thank you.</p>
                                </div>
                                <?php else: ?>
                                    <div class="check_mark">
                                        <div class="sa-icon sa-success animate">
                                            <span class="sa-line sa-tip animateSuccessTip"></span>
                                            <span class="sa-line sa-long animateSuccessLong"></span>
                                            <div class="sa-placeholder"></div>
                                            <div class="sa-fix"></div>
                                        </div>
                                    </div>
                                    <div class="single-message">
                                        <p>Checked In Successful</p>
                                        <p>Thank You <span><?= $participant['name'] ?></span> For Attending GDG Dhaka</p>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php elseif(isset($_GET['status']) && isset($_GET['participant'])): ?>
                                <div class="checkin-exceptions">
                                    <p>Opps! <span><?= $_GET['participant'] ?></span> is not registered yet for GDG Dhaka</p>
                                </div>
                            <?php endif; ?>

                            <div class="form-group agileits_w3layouts_form w3_agileits_margin checkin">
                                <div class="wthree_input">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input id="email" type="email" name="email" class="form-control" placeholder="Enter your email address" required />
                                </div>
                            </div>

                            <div class="clear"> </div>
                        </fieldset>
                        <p>
                            <button id="SaveAccount" class="btn btn-primary agileinfo_primary submit btn-min">Enter</button>
                        </p>
                        <p class="copy-right text-center">Powered By <a href="http://preneurlab.com/"><img src="http://trendki.com/task-manager/assets/img/prelab.png"/></a> </p>
                    </form>


                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->

<?php require_once 'footer.php' ?>