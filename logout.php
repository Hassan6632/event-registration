<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/auth-controller.php';

logoutMe();

unauthorizedUserRedirect('login.php');

?>
