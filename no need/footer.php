<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js"></script>


<script src="assets/js/main.js"></script>

<!--<script src="assets/js/jquery.formtowizard.js">

</script>
<script src="assets/js/jquery.validate.min.js"></script>


<script>
    $(function() {
        var $signupForm = $('#SignupForm');

        $signupForm.validate({
            errorElement: 'em'
        });

        $signupForm.formToWizard({
            submitButton: 'SaveAccount',
            nextBtnClass: 'btn btn-primary next',
            prevBtnClass: 'btn btn-default prev',
            buttonTag: 'button',
            validateBeforeNext: function(form, step) {
                var stepIsValid = true;
                var validator = form.validate();
                $(':input', step).each(function(index) {
                    var xy = validator.element(this);
                    stepIsValid = stepIsValid && (typeof xy == 'undefined' || xy);
                });
                return stepIsValid;
            },
            progress: function(i, count) {
                $('#progress-complete').width('' + (i / count * 100) + '%');
            }
        });
    });
</script>
<script>
    $("button").click(function() {
        $(".sa-success").addClass("hide");
        setTimeout(function() {
            $(".sa-success").removeClass("hide");
        }, 10);
    });
</script>-->


</body>

</html>
