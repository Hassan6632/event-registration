<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/auth-controller.php';

authorizedUserRedirect('dashboard.php');

if(isset($_POST['login']))
{
    userLogin();
}

?>

<?php setPageTitle('Login');  require_once 'header.php'; ?>

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Login
                </div>
                <div class="card-body">

                    <?php

                    if(isset($_GET['status']))
                    {
                        switch (safeString($_GET['status']))
                        {
                            case '4433':
                                echo alerts('Please enter a valid email address!', 'warning');
                                break;
                            case '4466':
                                echo alerts('All fields are required!', 'warning');
                                break;
                            case '4444':
                                echo alerts("Email is not registered yet!", 'warning');
                                break;
                            case '4455':
                                echo alerts('Your entered password is wrong!', 'warning');
                                break;

                        }
                    }

                    ?>

                    <form action="login.php" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="text" class="form-control" name="email" placeholder="Enter your email address" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter your password" required>
                        </div>
                        <button type="submit" name="login" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<?php require_once 'footer.php' ?>