<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/26/18
 * Time: 3:46 PM
 */


require_once 'core/Core.php';
require_once 'security/functions.php';
require_once 'db/db.php';

dbConfig($db);
date_default_timezone_set('Asia/Dhaka');

function participant_game()
{
    $email = $_POST['email'];
    $email = validEmail($email);


    # If Fields are fill
    if(!empty($email) == true)
    {
        # If Email is valid
        if($email['status'] === 'valid')
        {
            $where = array(
                'email = ?' => $email['valid_email'],
            );

            $participants = fetchFromDB('participants', $where);

            # If user exists
            if(!empty($participants))
            {
                
                $winners = array(7);
                
                $counter = fetchAllFromDB('counter');
                $new_count = $counter[0]['count'] + 1;
                
                
                $counter_columns = array(
                    'count = ?' => $new_count,
                ); 
                
                
                $count_success = updateAllDB('counter', $counter_columns);
                
                if($count_success)
                {
                    if($participants[0]['game'] == 3)
                    {
                         header('Location: http://preneurlab.digital/event/game/index.php?day=sun');
                    }
                    else
                    {
                        $participants_columns = array(
                                    'game = ?' => 2,
                                );
                            
                        $base_columns = array(
                            'email =?' => $email['valid_email']
                        );
                        
                        updateDB('participants', $participants_columns, $base_columns);
                        
                        
                        if(in_array($new_count,$winners))
                        {
                            $participants_columns = array(
                                    'game = ?' => 3,
                                );
                            
                            $base_columns = array(
                                'email =?' => $email['valid_email']
                            );
                            
                             updateDB('participants', $participants_columns, $base_columns);
                             
                             header('Location: http://preneurlab.digital/event/game/index.php?day=sun');
                        }
                        else
                        {
                            header('Location: http://preneurlab.digital/event/game/index.php');
                        }
                    
                    }
                    
                }
              
                
            }
            else
            {
                # If participants not exists
                header('Location: game.php?status=4444&participant='.$email['valid_email']);
            }
        }
        else
        {
            # If Email  is invalid
            header('Location: game.php?status=4433');
        }
    }
    else
    {
        # If Fields are  empty
        header('Location: game.php?status=4466');
    }
}


