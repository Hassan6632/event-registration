<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/26/18
 * Time: 6:42 PM
 */


require_once 'core/Core.php';
require_once 'db/db.php';

dbConfig($db);

function participants_info($category)
{
    switch ($category)
    {
        case 'all':
            return fetchAllFromDB('participants');
            break;
        case 'attended':
            return fetchFromDB('participants', array('attended =?' => 1));
            break;
        case 'absents':
            return fetchFromDB('participants', array('attended =?' => 0));
            break;
    }
}