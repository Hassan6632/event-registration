<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/26/18
 * Time: 3:46 PM
 */


require_once 'core/Core.php';
require_once 'security/functions.php';
require_once 'db/db.php';

dbConfig($db);
date_default_timezone_set('Asia/Dhaka');

function check_in_participant()
{
    $email = $_POST['email'];
    $email = validEmail($email);

    # If Fields are fill
    if(!empty($email) == true)
    {
        # If Email is valid
        if($email['status'] === 'valid')
        {
            $where = array(
                'email = ?' => $email['valid_email'],
            );

            $participants = fetchFromDB('participants', $where);

            # If user exists
            if(!empty($participants))
            {
                $columns = array(
                    'attended = ?' => 1,
                    'attend_time =?' => date('Y-m-d H:i:s'),
                ); 

                $base_columns = array(
                    'email =?' => $email['valid_email']
                );

                $checked_in_status = updateDB('participants', $columns, $base_columns);

                return ($checked_in_status == true) ? $participants[0] : $participants[0];
            }
            else
            {
                # If participants not exists
                header('Location: checkin.php?status=4444&participant='.$email['valid_email']);
            }
        }
        else
        {
            # If Email  is invalid
            header('Location: checkin.php?status=4433');
        }
    }
    else
    {
        # If Fields are  empty
        header('Location: checkin.php?status=4466');
    }
}


