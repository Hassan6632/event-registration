<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/21/18
 * Time: 2:30 PM
 */


require_once 'core/Core.php';
require_once 'security/functions.php';
require_once 'db/db.php';

function userLogin()
{
    $email = $_POST['email'];
    $email = validEmail($email);
    $password = $_POST['password'];

    $not_empty = !empty($email) && !empty($password);

    # If Fields are fill
    if($not_empty == true)
    {
        # If Email is valid
        if($email['status'] === 'valid')
        {
            $where = array(
                'email = ?' => $email['valid_email'],
            );

            $user = fetchFromDB('users', $where);

            # If user exists
            if(!empty($user))
            {
                if(password_verify($password, $user[0]['password']))
                {
                    $_SESSION['authorized'] = $user[0]['id'];
                    $_SESSION['csrf_token'] = encryption(uniqid(true));

                    if($_SESSION['authorized'])
                    {
                        header('Location: dashboard.php');
                    }

                }
                else
                {
                    # If password is wrong
                    header('Location: login.php?status=4455');
                }
            }
            else
            {
                # If user not exists
                header('Location: login.php?status=4444');
            }

        }
        else
        {
            # If Email  is invalid
            header('Location: login.php?status=4433');
        }

    }
    else
    {
        # If Fields are  empty
        header('Location: login.php?status=4466');
    }
}

function userReg()
{
    $name = safeString($_POST['name']);
    $email = validEmail($_POST['email']);
    $password = password_hash($_POST['password'], 1);

    $not_empty = !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['password']);

    if($not_empty == true)
    {
        if($email['status'] === 'valid')
        {
            $user_reg = array(
                NULL,
                $name,
                $email['valid_email'],
                $password
            );

            $user_reg_status = storeInDB('users', $user_reg);

            if($user_reg_status == true)
            {
                header('Location: register.php?status=1111');
            }
        }
        else
        {
            // If Email  is invalid
            header('Location: register.php?status=4433');
        }

    }
    else
    {
        // If Fields are  empty
        header('Location: register.php?status=4466');
    }
}
