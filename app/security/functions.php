<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/29/18
 * Time: 11:48 AM
 */


function encryption($string)
{
    return openssl_encrypt($string,"AES-128-ECB",'hqvDiEfHKfSYWscgqFVEJw==');
}

function decryption($string)
{
    return openssl_decrypt($string,"AES-128-ECB",'hqvDiEfHKfSYWscgqFVEJw==');
}

function csrfToken()
{
    return "<input type='hidden' name='csrf_token' value='".$_SESSION['csrf_token']."'>";
}

function verifyCsrfToken($post_csrf_token)
{
    return ($post_csrf_token === 'ZjlWSu/QAg0F4aKV40hsCA==') ? true : false;
}

function safeString($unsafe_string)
{
    return filter_var(stripslashes($unsafe_string), FILTER_SANITIZE_STRING);
}

function validEmail($email)
{
    if(filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $valid_email = safeString(filter_var($email, FILTER_SANITIZE_EMAIL));
        return array('status' => 'valid', 'valid_email' => $valid_email);
    }
    else
    {
        return array('status' => 'invalid', 'valid_email' => false);
    }
}

function numberInt($string)
{
    return filter_var($string, FILTER_SANITIZE_NUMBER_INT);
}

function unauthorizedUserRedirect($url)
{
    if (!isset($_SESSION['authorized']))
    {
        header('Location:'.$url);
    }
}

function authorizedUserRedirect($url)
{
    if (isset($_SESSION['authorized']))
    {
        header('Location:'.$url);
    }
}

function logoutMe()
{
    session_unset();
    session_destroy();

    if (!isset($_SESSION['authorized']))
    {
        header('Location: index.php');
    }
}

