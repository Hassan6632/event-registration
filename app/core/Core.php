<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/19/18
 * Time: 5:05 PM
 */

/*
 * FILTER_SANITIZE_STRING : Strip tags, optionally strip or encode special characters.
 * FILTER_SANITIZE_NUMBER_INT : Remove all characters except digits, plus and minus sign.
 */

$db = null;

function dbConfig($config)
{
    global $db;
    $db = $config;
}


function storeInDB($table,$values)
{
    global $db;

    $params = array_fill(0, count($values), '?');
    $sql = "INSERT INTO ".$table." VALUES(".implode(',', $params).")";
    $stmt = $db->prepare($sql);
    $stmt->execute($values);
    return ($stmt->rowCount()) ? true : false;

}

function fetchFromDB($table,$where)
{
    global $db;

    $sql = "SELECT * FROM ".$table." WHERE ".implode('', array_keys($where));
    $stmt = $db->prepare($sql);
    $stmt->execute(array_values($where));
    return $stmt->fetchAll(2);
}

function fetchAllFromDB($table)
{
    global $db;

    $sql = "SELECT * FROM ".$table;
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(2);
}

function updateDB($table,$columns,$base_columns)
{
    global $db;

    $final_columns = array_merge($columns, $base_columns);
    $sql = "UPDATE ".$table." SET ".implode(',', array_keys($columns))." WHERE ".implode(',', array_keys($base_columns));
    $stmt = $db->prepare($sql);
    $stmt->execute(array_values($final_columns));

    return ($stmt->rowCount()) ? true : false;
}

function updateAllDB($table,$columns)
{
    global $db;

    $sql = "UPDATE ".$table." SET ".implode(',', array_keys($columns));
    $stmt = $db->prepare($sql);
    $stmt->execute(array_values($columns));

    return ($stmt->rowCount()) ? true : false;
}

function dbClose()
{
    global $db;
    $db = null;
}

