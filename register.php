<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/auth-controller.php';

unauthorizedUserRedirect('login.php');

if(isset($_POST['register']))
{
    userReg();
}

?>

<?php setPageTitle('Register');  require_once 'header.php';?>

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Register
                </div>
                <div class="card-body">


                    <?php

                    if(isset($_GET['status']))
                    {
                        switch (safeString($_GET['status']))
                        {
                            case '4433':
                                echo alert('Please enter a valid email address', 'warning');
                                break;
                            case '4466':
                                echo alert('All fields are required!', 'warning');
                                break;
                            case '1111':
                                echo alert('Account created successfully!', 'success');
                                break;

                        }
                    }

                    ?>

                    <form action="register.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Full name</label>
                            <input type="text" class="form-control" name="name" placeholder="What's your full name?" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Your email address</label>
                            <input type="text" class="form-control" name="email" placeholder="What's your email address?" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Create a password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <button type="submit" name="register" class="btn btn-primary">Register</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

<?php require_once 'footer.php' ?>