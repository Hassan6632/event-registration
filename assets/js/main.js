(function ($) {
    "use strict";

    jQuery(document).ready(function () {


        /*  HOME Carousel
               ----------------------------------*/
        $(".all-reg-frm").owlCarousel({
            items: 1,
            dots: true,
            autoplay: false,
            loop: false,
            nav: true,

        });

        $(".event-info-carousel").owlCarousel({
            items: 3,
            dots: true,
            autoplay: true,
            loop: true,
            nav: false,

        });


        /*  - Scroll-top 
      	---------------------------------------------*/
        jQuery(window).on('scroll', function () {
            var scrollTop = jQuery(this).scrollTop();
            if (scrollTop > 400) {
                jQuery('.top').fadeIn();
            } else {
                jQuery('.top').fadeOut();
            }
        });

        jQuery('.top').on('click', function () {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);

            return false;
        });






    });


    jQuery(window).on("load", function () {
        /*  - Pre Loader
        ---------------------------------------------*/
        $(".pre-loader-area").fadeOut();

    });


}(jQuery));
