<?php

require_once 'app/auth-controller.php';
require_once 'app/checkin-controller.php';
require_once 'app/general/functions.php';
require_once 'app/security/functions.php';

unauthorizedUserRedirect('../login.php');

$participant = null;

if(isset($_POST['email']))
{
    $participant = check_in_participant();
}

?>

<?php setPageTitle('Check In');  require_once 'header.php'; ?>
    <style>
        #step0commands {
            display: none;
        }

        .agile_form {
            padding: 3em;
            background: #fff;
            padding-bottom: 71px !important;
        }
        
@media (max-width: 575px) {
    .checkin {
        width: 100%;
        margin-left: 0%;
    }
    .checkin input {
        width: 100%;
    }
    .thanks-messsage {
        display: block;
    }
    .check_mark {
        margin: 0 auto;
    }
    .single-message {
        margin-left: 0;
        margin-top: 0;
        margin-bottom: 20px;
        text-align: center;
    }
    .btn-min {
        margin-right: 52px;
    }
    .wthree_input input[type="text"], .w3ls_description textarea, .wthree_input select, .wthree_input input[type="email"], .wthree_input input[type="password"]{
        width:100%;
    }
}
    </style>

    <!--    [ Strat Section Title Area]-->
    <section id="reg-frm">
        <div class="section-padding">
            <div class="main">
                <h1>GDG Dhaka</h1>
                <div class="w3_agile_main_grids">
                    <form id="SignupForm" action="checkin.php" class="agile_form" method="post">
                        <fieldset>
                            <h3><i class="fas fa-map-marker-alt"></i>Participants Check In</h3>

                            <?php if(!empty($participant)): ?>
                            <div class="thanks-messsage">
                                <?php if($participant['attended'] == 1): ?>
                                <div class="checkin-exceptions">
                                    <p><span><?= $participant['name'] ?></span> already checked in .Thank you.</p>
                                </div>
                                <?php else: ?>
                                    <div class="check_mark">
                                        <div class="sa-icon sa-success animate">
                                            <span class="sa-line sa-tip animateSuccessTip"></span>
                                            <span class="sa-line sa-long animateSuccessLong"></span>
                                            <div class="sa-placeholder"></div>
                                            <div class="sa-fix"></div>
                                        </div>
                                    </div>
                                    <div class="single-message">
                                        <p>Checked In Successful</p>
                                        <p>Thank You <span><?= $participant['name'] ?></span> For Attending GDG Dhaka</p>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php elseif(isset($_GET['status']) && isset($_GET['participant'])): ?>
                                <div class="checkin-exceptions">
                                    <p>Opps! <span><?= $_GET['participant'] ?></span> is not registered yet for GDG Dhaka</p>
                                </div>
                            <?php endif; ?>

                            <div class="form-group agileits_w3layouts_form w3_agileits_margin checkin">
                                <div class="wthree_input">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input id="email" type="email" name="email" class="form-control" placeholder="Enter your email address" required />
                                </div>
                            </div>

                            <div class="clear"> </div>
                        </fieldset>
                        <p class="rees-btn">
                            <button id="SaveAccount" class="btn btn-primary agileinfo_primary submit btn-min">CheckIn</button>
                        </p>
                    </form>

                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->

<?php require_once 'footer.php' ?>