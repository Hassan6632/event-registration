<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';

//unauthorizedUserRedirect('../login.php');

?>

    <?php setPageTitle('Page Title Here');  require_once 'header.php'; ?>

    <!--    [ Strat Section Title Area]-->
    <section id="login">
        <div class="section-paddings">
            <div class="container">
                <div class="event-info-carousel owl-carousel">
                    <div class="login-content d-table">
                        <div class="login-txt d-table-cell">
                            <div class="single-event">
                                <div class="card">
                                    <div class="card-header">Google I/O Developer Confe</div>
                                    <div class="card-body">
                                        <div class="event-tit">
                                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, necessitatibus.</h3>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-lg-7 no-padding">
                                                <div class="time-table">
                                                    <h5>Date: <span>01.08.2018</span></h5>
                                                    <h5>Vanue: <span>BCT Panthapath</span></h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <div class="location">
                                                    <div class="map-show">
                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.2654264955077!2d90.37915151489527!3d23.73791249517037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c0af491c4611%3A0x8c8cfd1af5649503!2sPreneurLab!5e0!3m2!1sen!2sbd!4v1533122980318" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                    </div>
                                                    <a target="_blank" href="https://www.google.com/maps/@23.7330573,90.3707983,15z" class="gmaip-link text-center">
                                               
                                                <i class="icofont">location_pin</i>
                                                <p>Location</p>
                                            </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <!--    [ Strat Section Area]-->
    <!--    [Finish Section Area]-->
    <?php require_once 'footer.php' ?>
