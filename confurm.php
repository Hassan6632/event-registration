<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';

//unauthorizedUserRedirect('../login.php');

?>

    <?php setPageTitle('Page Title Here');  require_once 'header.php'; ?>

    <!--    [ Strat Section Title Area]-->
    <section id="login">
        <div class="section-paddings">
            <div class="container">
                <div class="login-content d-table">
                    <div class="login-txt d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                                <form action="">
                                    <div class="log-frm going-confurm">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            
                                                            <span>Going <i class="icofont">heart_eyes</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                        <input name="group1" type="radio" checked />
                                                        <span>Not Sure <i class="icofont">nerd_smile</i></span>
                                                    </label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="single-going">
                                                    <p>
                                                        <label>
                                                            <input name="group1" type="radio" checked />
                                                            <span>Not Doing <i class="icofont">crying</i></span>
                                                        </label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <!--    [ Strat Section Area]-->
    <!--    [Finish Section Area]-->
    <?php require_once 'footer.php' ?>
