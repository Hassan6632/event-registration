<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/participant-controller.php';


unauthorizedUserRedirect('login.php');

$all_participants = participants_info('all');
$attended_participants = participants_info('attended');
$absent_participants = participants_info('absents');

?>

<?php setPageTitle('Dashboard');  require_once 'header.php'; ?>

    <section id="event">
        <div class="section-padding">
            <div class="container">
                <div class="bg-icon"><i class="icofont">ui_keyboard</i></div>
                <div class="page-title text-center">
                    <h3>GDG Dhaka Event Overview</h3>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-3">
                        <a class="sbtn-a" href="participants.php">
                            <div class="single-btn text-center">
                                <h6>Registered Participants</h6>
                                <p><?= count($all_participants)?></p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a class="sbtn-a" href="attended.php">
                            <div class="single-btn text-center">
                                <h6>Attended Participants</h6>
                                <p><?= count($attended_participants)?></p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a class="sbtn-a" href="absents.php">
                            <div class="single-btn text-center">
                                <h6>Absent Participants</h6>
                                <p><?= count($absent_participants)?></p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php require_once 'footer.php' ?>