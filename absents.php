<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/participant-controller.php';

unauthorizedUserRedirect('login.php');

$all_participants = participants_info('absents');

?>

<?php setPageTitle('Participants');  require_once 'header.php'; ?>

    <section id="event">
            <div class="container">
                <div class="page-title text-center mt-5">
                    <h3>Attended Participants</h3>
                </div>

                <div class="ev-list">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Institutions</th>
                                    <th scope="col">T-Shirt Size</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($all_participants as $participant): ?>
                                <tr>
                                    <th scope="row"><?= $participant['name'] ?></th>
                                    <td><?= $participant['email'] ?></td>
                                    <td><?= $participant['mobile'] ?></td>
                                    <td><?= $participant['institutions'] ?></td>
                                    <td><?= $participant['tshirt_size'] ?>
                                </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
<?php include('footer.php'); ?>