<?php

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';

//unauthorizedUserRedirect('../login.php');

?>

    <?php setPageTitle('Page Title Here');  require_once 'header.php'; ?>

    <!--    [ Strat Section Title Area]-->
    <section id="login">
        <div class="section-paddings">
            <div class="container">
                <div class="login-content d-table">
                    <div class="login-txt d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                                <form action="">
                                    <div class="log-frm">
                                        <div class="input-field">
                                            <input id="email" type="email" class="validate">
                                            <label for="email">Email</label>
                                            <span class="helper-text" data-error="wrong" data-success="right">Helper text</span>
                                        </div>
                                        <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <!--    [ Strat Section Area]-->
    <!--    [Finish Section Area]-->
    <?php require_once 'footer.php' ?>
